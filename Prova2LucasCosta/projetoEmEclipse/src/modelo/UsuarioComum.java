package modelo;

public class UsuarioComum { 
    
    private Comentario comentario;
    private Perfil perfil;
    protected String tipoUsuario;
    
    public UsuarioComum()
    {
    	tipoUsuario = "UsuarioComum";
    }
    

    public Comentario getComentario() {
        return comentario;
    }

    public void setComentario(Comentario comentario) {
        this.comentario = comentario;
    }
    
    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }   
    
    public Debate criarDebate(String tituloDebate)
    {
    	Debate umDebate = new Debate();
        umDebate.setTitulo(tituloDebate);
        
        return umDebate;
    }
    
    public Debate criarDebate(String titulo, String descricao)
    {
        Debate umDebate = new Debate();
        umDebate.setTitulo(titulo);
        umDebate.setDescricao(descricao);
        
        return umDebate;
    }
    
    public Comentario sugerirMensagem(String umaMensagem)
    {
        String dataAtual = "data atual";
        String horaAtual = "hora atual"; 
        
        Comentario umComentario = new Comentario(perfil.getNome(), umaMensagem , dataAtual, horaAtual);
        return umComentario;
    }
}
