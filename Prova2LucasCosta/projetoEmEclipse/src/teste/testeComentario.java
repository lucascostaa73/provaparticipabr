package teste;

import static org.junit.Assert.*;

import org.junit.Test;
import modelo.Comentario;


public class testeComentario {
	
	@before
	Comentario umComentario = new Comentario("nomeAutor","mensagem","umaData","umaHora");
	
	@Test
	public void testGetAutor() {
		AssertEquals("nomeAutor",umComentario.getAutor());
		
	}
	public void testGetData() {
		AssertEquals("umaData",umComentario.getData());
		
	}
	public void testGetHora() {
		AssertEquals("umaHora",umComentario.getHora());
		
	}
	public void testGetMensagem() {
		AssertEquals("nomeMensagem",umComentario.getMensagem());
		
	}

}
