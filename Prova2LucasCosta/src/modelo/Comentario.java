/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author alunos
 */
public class Comentario {
    
    private String mensagem;
    private String autor;
    private String data;
    private String hora;
    
    Comentario(String umAutor,String umaMensagem, String umaData, String umaHora)
    {
        autor = umAutor;
        mensagem = umaMensagem;
        data = umaData;
        hora = umaHora;
    }

    public String getAutor() {
        return autor;
    }

    public String getData() {
        return data;
    }

    public String getHora() {
        return hora;
    }

    public String getMensagem() {
        return mensagem;
    }

    
    
}
