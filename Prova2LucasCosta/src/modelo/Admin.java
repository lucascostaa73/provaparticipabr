/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

public class Admin extends UsuarioModerador{
    
    private String comunicado;
    ArrayList<UsuarioComum> controleUsuario = new ArrayList<UsuarioComum>();
    
    
    public void removerUsuario(UsuarioComum umUsuario)
    {
        controleUsuario.remove(umUsuario);
    }
    
    public void adicionarUsuario(UsuarioComum umUsuario)
    {
        controleUsuario.add(umUsuario);
    }
    
    public String comunicarUsuario(UsuarioComum umUsuario)
    {
        return comunicado;
    }
    
    
}
